# Choqok
KDE Micro-Blogging Client.

Currently supports Mastodon, Pleroma, Twitter, Pump.io, GNU Social, and Friendica.

### Authors
 - Nekobit \<me _at_ ow.nekobit.net\>
 - Mehrdad Momeny \<mehrdad.momeny _at_ gmail.com\>
 - Andrey Esin \<gmlastik _at_ gmail.com\>
 - Andrea Scarpino \<scarpino _at_ kde.org\>

### License
 GNU GPL v2, v3 or Later

# Building
### Requirements
 - CMake 2.8.12
 - Qt 5.9
 - KDE Frameworks libraries 5.6
 - QCA2-Qt5 library

### Building

    $ cd choqok-src-root-dir # [It's choqok-VERSION]
    $ mkdir build
    $ cd build
    $ cmake -DKDE_INSTALL_USE_QT_SYS_PATHS=ON ..
    $ make

### Installing

    # make install # run as root!

And to uninstall...

    # make uninstall # run as root!

### Debugging

You can enable Choqok debugging by setting `QT_LOGGING_RULES` to `org.kde.choqok.debug=true` with:

    export QT_LOGGING_RULES="org.kde.choqok.debug=true"

# Contributing / Questions
Feel free to email a developer, or open an Issue Ticket at [bugs.kde.org](https://bugs.kde.org/)

Or maybe ask a developer on social media if discovered :)