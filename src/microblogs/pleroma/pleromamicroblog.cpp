/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "pleromamicroblog.h"
#include "account/accountmanager.h"
#include "libchoqokdebug.h"
#include "pleromaaccount.h"
#include "pleromaeditaccountwidget.h"
#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(PleromaMicroblog, "choqok_pleroma.json")

PleromaMicroblog::PleromaMicroblog(QObject *parent, const QVariantList &args)
    : Microblog(QStringLiteral("Pleroma"), parent)
{
    setServiceName(QLatin1String("Pleroma"));
    setServiceHomepageUrl(QLatin1String("https://pleroma.social"));
    QStringList timelineNames;
    timelineNames << QLatin1String("Home") << QLatin1String("Local") << QLatin1String("Federated") << QLatin1String("Favourites");
    setTimelineNames(timelineNames);
    //    setTimelinesInfo();
}

Choqok::Account *PleromaMicroblog::accountFactory(const QString &alias)
{
    PleromaAccount *acc = qobject_cast<PleromaAccount *>(
        Choqok::AccountManager::self()->findAccount(alias));
    if (!acc) {
        return new PleromaAccount(this, alias);
    } else {
        qCDebug(CHOQOK) << "Cannot create a new PleromaAccount!";
        return nullptr;
    }
}

ChoqokEditAccountWidget *PleromaMicroblog::editAccountWidgetFactory(Choqok::Account *account,
                                                                    QWidget *parent)
{
    PleromaAccount *acc = qobject_cast<PleromaAccount *>(account);
    if (acc || !account) {
        return new PleromaEditAccountWidget(this, acc, parent);
    } else {
        qCDebug(CHOQOK) << "Account passed here was not a valid PleromaAccount!";
        return nullptr;
    }
}

#include "pleromamicroblog.moc"
