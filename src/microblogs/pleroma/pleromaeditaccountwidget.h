/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include "editaccountwidget.h"
#include "ui_pleromaeditaccountwidget.h"
#include <QUrlQuery>

class PleromaAccount;
class PleromaMicroblog;

class PleromaEditAccountWidget : public ChoqokEditAccountWidget,
                                 Ui::PleromaEditAccountWidget
{
    Q_OBJECT
public:
    explicit PleromaEditAccountWidget(PleromaMicroblog *microblog,
                                      PleromaAccount *account,
                                      QWidget *parent);
    virtual ~PleromaEditAccountWidget() = default;

    virtual Choqok::Account *apply() override;

private:
    PleromaAccount *m_account;
};
