/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include "account/account.h"
#include "choqoktypes.h"

class PleromaMicroblog;

class PleromaAccount : public Choqok::Account
{
    Q_OBJECT
public:
    explicit PleromaAccount(PleromaMicroblog *parent, const QString &alias);
    ~PleromaAccount() = default;
};
