/*
  This file is part of Choqok, the KDE micro-blogging client

  SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "pleromaaccount.h"
#include "pleromamicroblog.h"

PleromaAccount::PleromaAccount(PleromaMicroblog *parent, const QString &alias)
    : Account(parent, alias)
{
}
