/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include "microblog/microblog.h"

class PleromaMicroblog : public Choqok::Microblog
{
    Q_OBJECT
public:
    explicit PleromaMicroblog(QObject *parent, const QVariantList &args);
    virtual ~PleromaMicroblog() = default;

    Choqok::Account *accountFactory(const QString &alias) override final;
    ChoqokEditAccountWidget *editAccountWidgetFactory(Choqok::Account *account,
                                                      QWidget *parent) override final;
};
