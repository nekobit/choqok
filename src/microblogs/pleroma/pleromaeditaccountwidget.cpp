/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "pleromaeditaccountwidget.h"
#include "pleromaaccount.h"

PleromaEditAccountWidget::PleromaEditAccountWidget(PleromaMicroblog *microblog,
                                                   PleromaAccount *account,
                                                   QWidget *parent)
    : ChoqokEditAccountWidget(account, parent)
    , m_account{account}
{
    setupUi(this);
}

Choqok::Account *PleromaEditAccountWidget::apply()
{
    m_account->setAlias(kcfg_alias->text());
    m_account->setUsername(kcfg_acct->text());
    //    m_account->setTokenSecret(m_account->oAuth()->token());
    m_account->writeConfig();
    //    saveTimelinesTable();
    return m_account;
}
