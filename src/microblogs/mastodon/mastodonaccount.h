/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef MASTODONACCOUNT_H
#define MASTODONACCOUNT_H

#include "account/account.h"
#include "choqoktypes.h"

class MastodonMicroblog;

class MastodonAccount : public Choqok::Account
{
    Q_OBJECT
public:
    explicit MastodonAccount(MastodonMicroblog *parent, const QString &alias);
    ~MastodonAccount();

    virtual void writeConfig() override;

private:
    QString m_consumerKey;
    QString m_consumerSecret;
    QString m_host;
    uint m_id;
    QString m_tokenSecret;
    QStringList m_followers;
    QStringList m_following;
    QVariantList m_lists;
    // MastodonOAuth *m_oAuth;
    QStringList m_timelineNames;
};

#endif // MASTODONACCOUNT_H
