/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef MASTODONMICROBLOG_H
#define MASTODONMICROBLOG_H

#include "microblog/microblog.h"
#include "plugin.h"
#include <QNetworkAccessManager>

class QUrl;
class KJob;
class MastodonAccount;
class MastodonPost;

class MastodonMicroblog : public Choqok::Microblog
{
    Q_OBJECT
public:
    explicit MastodonMicroblog(QObject *parent, const QVariantList &args);
    virtual ~MastodonMicroblog() = default;

    virtual void aboutToUnload() override;

    Choqok::Account *accountFactory(const QString &alias) override final;
    ChoqokEditAccountWidget *editAccountWidgetFactory(Choqok::Account *account,
                                                      QWidget *parent) override final;
};

#endif // MASTODONMICROBLOG_H
