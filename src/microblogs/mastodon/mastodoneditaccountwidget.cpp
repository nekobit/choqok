/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "mastodoneditaccountwidget.h"

#include <QCheckBox>
#include <QCoreApplication>
#include <QEventLoop>
#include <QInputDialog>
#include <QJsonDocument>
#include <QPushButton>
#include <QUrl>

#include <KIO/AccessManager>
#include <KIO/StoredTransferJob>
#include <KMessageBox>

#include "account/accountmanager.h"
#include "choqoktools.h"

#include "mastodonaccount.h"
#include "mastodondebug.h"
#include "mastodonmicroblog.h"

MastodonEditAccountWidget::MastodonEditAccountWidget(MastodonMicroblog *microblog,
                                                     MastodonAccount *account,
                                                     QWidget *parent)
    : ChoqokEditAccountWidget(account, parent)
    , m_account(account)
{
    setupUi(this);
}

Choqok::Account *MastodonEditAccountWidget::apply()
{
    return m_account;
}
