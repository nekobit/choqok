/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "mastodonmicroblog.h"

#include <QAction>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMenu>
#include <QMimeDatabase>
#include <QTextDocument>

#include <KIO/StoredTransferJob>

#include "account/accountmanager.h"
#include "application.h"
#include "choqokappearancesettings.h"
#include "choqokbehaviorsettings.h"
#include "notifymanager.h"
#include "postwidget.h"

#include "mastodonaccount.h"
#include "mastodondebug.h"
#include "mastodoneditaccountwidget.h"
#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(MastodonMicroblog, "choqok_mastodon.json")

namespace
{

const QString urls[] = {
    QLatin1String("/api/v1/timelines/home"),
    QLatin1String("/api/v1/timelines/public"),
    QLatin1String("/api/v1/favourites"),
};
}

MastodonMicroblog::MastodonMicroblog(QObject *parent, const QVariantList &args)
    : Microblog(QStringLiteral("Mastodon"), parent)
{
    Q_UNUSED(args)
    setServiceName(QLatin1String("Mastodon"));
    setServiceHomepageUrl(QLatin1String("https://mastodon.social"));
    QStringList timelineNames;
    timelineNames << QLatin1String("Home") << QLatin1String("Local") << QLatin1String("Federated") << QLatin1String("Favourites");
    setTimelineNames(timelineNames);
}

void MastodonMicroblog::aboutToUnload()
{
}

Choqok::Account *MastodonMicroblog::accountFactory(const QString &alias)
{
    MastodonAccount *acc = qobject_cast<MastodonAccount *>(
        Choqok::AccountManager::self()->findAccount(alias));
    if (!acc) {
        return new MastodonAccount(this, alias);
    } else {
        qCDebug(CHOQOK) << "Cannot create a new MastodonAccount!";
        return nullptr;
    }
}

ChoqokEditAccountWidget *MastodonMicroblog::editAccountWidgetFactory(Choqok::Account *account,
                                                                     QWidget *parent)
{
    MastodonAccount *acc = qobject_cast<MastodonAccount *>(account);
    if (acc || !account) {
        return new MastodonEditAccountWidget(this, acc, parent);
    } else {
        qCDebug(CHOQOK) << "Account passed here was not a valid MastodonAccount!";
        return nullptr;
    }
}

#include "mastodonmicroblog.moc"
