/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2017 Andrea Scarpino <scarpino@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef MASTODONEDITACCOUNTWIDGET_H
#define MASTODONEDITACCOUNTWIDGET_H

#include "editaccountwidget.h"

#include <QUrlQuery>

#include "ui_mastodoneditaccountwidget.h"

class MastodonAccount;
class MastodonMicroblog;

class MastodonEditAccountWidget : public ChoqokEditAccountWidget,
                                  Ui::MastodonEditAccountWidget
{
    Q_OBJECT
public:
    MastodonEditAccountWidget(MastodonMicroblog *microblog, MastodonAccount *account,
                              QWidget *parent);
    virtual ~MastodonEditAccountWidget() = default;

    virtual Choqok::Account *apply() override;

private Q_SLOTS:

private:
    MastodonAccount *m_account;
    bool isAuthenticated;
};

#endif // MASTODONEDITACCOUNTWIDGET_H
