/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2011-2012 Mehrdad Momeny <mehrdad.momeny@gmail.com>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef NOTIFY_H
#define NOTIFY_H

#include <QMap>
#include <QPoint>
#include <QPointer>
#include <QQueue>
#include <QTimer>

#include "plugin.h"

class Notification;
namespace Choqok
{
namespace UI
{
class StatusWidget;
}
class Account;
}

class Notify : public Choqok::Plugin
{
    Q_OBJECT
public:
    Notify(QObject *parent, const QList<QVariant> &args);
    ~Notify();

protected Q_SLOTS:
    void slotNewStatusWidgetAdded(Choqok::UI::StatusWidget *, Choqok::Account *, QString);
    void notifyNextPost();
    void stopNotifications();
    void slotPostReaded();

private:
    void notify(QPointer<Choqok::UI::StatusWidget> post);
    void hideLastNotificationAndShowThis(Notification *nextNotificationToShow = nullptr);

    QTimer timer;
    QMap<QString, QStringList> accountsList;
    QQueue<Choqok::UI::StatusWidget *> postQueueToNotify;
    Notification *notification;
    QPoint notifyPosition;
};

#endif // NOTIFY_H
