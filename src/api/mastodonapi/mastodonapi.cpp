/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "mastodonapi.h"
#include "microblog/microblogapi.h"

using namespace Choqok::API;

Mastodon::Mastodon(QObject *parent /* = nullptr */)
    : MicroblogApi(QStringLiteral("pleroma.soykaf.com"), parent)
{
}
