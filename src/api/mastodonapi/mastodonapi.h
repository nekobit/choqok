/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include "http/httpapi.h"
#include "mastodonapi_export.h"
#include "microblog/microblogapi.h"
#include <QObject>
#include <QString>

namespace Choqok
{

/**
   @brief Mastodon API implementation and base for Pleroma

   @author Nekobit \<me@ow.nekobit.net\>
*/

namespace API
{
class CHOQOK_EXPORT Mastodon : public API::MicroblogApi
{
    Q_OBJECT
public:
    Mastodon(QObject *parent = nullptr);
    // Can be inherited by Pleroma or possibly others
    virtual ~Mastodon() = default;
};
}
}
