/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2008-2012 Mehrdad Momeny <mehrdad.momeny@gmail.com>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "mediamanager.h"

#include <QApplication>
#include <QHash>
#include <QIcon>
#include <QMimeDatabase>
#include <QSharedPointer>

#include <KIO/StoredTransferJob>
#include <KLocalizedString>
#include <KMessageBox>

#include "choqokbehaviorsettings.h"
#include "choqokuiglobal.h"
#include "libchoqokdebug.h"
#include "pluginmanager.h"

using namespace Choqok;

MediaManager::MediaManager()
    : QThread(qApp)
    , emoticons(KEmoticons().theme())
    // 30 mb
    , cache(QLatin1String("choqok-userimages"), 30 * 1000000)
    , queue()
    , cacheQueue()
    , cacheMutex()
    , defImage(QIcon::fromTheme(QLatin1String("image-loading")).pixmap(48))
    , uploader(nullptr)
{
}

MediaManager::~MediaManager()
{
}

MediaManager *MediaManager::mSelf = nullptr;

MediaManager *MediaManager::self()
{
    if (!mSelf) {
        mSelf = new MediaManager;
    }
    return mSelf;
}

QPixmap &MediaManager::defaultImage()
{
    return defImage;
}

QString MediaManager::parseEmoticons(const QString &text)
{
    return emoticons.parseEmoticons(text, KEmoticonsTheme::DefaultParse, QStringList() << QLatin1String("(e)"));
}

QPixmap MediaManager::fetchImage(const QUrl &remoteUrl)
{
    QPixmap p;
    if (cache.findPixmap(remoteUrl.toDisplayString(), &p)) {
        Q_EMIT imageFetched(remoteUrl, p);
    } else {
        if (queue.values().contains(remoteUrl)) {
            /// The file is on the way, wait to download complete.
            return p;
        }
        KIO::StoredTransferJob *job = KIO::storedGet(remoteUrl, KIO::NoReload, KIO::HideProgressInfo);
        if (!job) {
            qCCritical(CHOQOK) << "Cannot create a FileCopyJob!";
            Q_EMIT fetchError(remoteUrl, i18n("Cannot create a KDE Job. Please check your installation."));
            return p;
        }
        queue.insert(job, remoteUrl);
        connect(job, &KIO::StoredTransferJob::result, this, &MediaManager::slotImageFetched);
        job->start();
    }
    return p;
}

void MediaManager::slotImageFetched(KJob *job)
{
    KIO::StoredTransferJob *baseJob = qobject_cast<KIO::StoredTransferJob *>(job);
    QUrl remote = queue.value(job);
    queue.remove(job);

    int responseCode = 0;
    if (baseJob->metaData().contains(QStringLiteral("responsecode"))) {
        responseCode = baseJob->queryMetaData(QStringLiteral("responsecode")).toInt();
    }

    if (job->error() || (responseCode > 399 && responseCode < 600)) {
        qCCritical(CHOQOK) << "Job error:" << job->error() << "\t" << job->errorString();
        qCCritical(CHOQOK) << "HTTP response code" << responseCode;
        QString errMsg = i18n("Cannot download image from %1.", job->errorString());
        Q_EMIT fetchError(remote, errMsg);
    } else {
        // Loading and caching requres some heavy work which can lock,
        // so throw into a thread
        ImageFetchData imgData{baseJob->data(), remote};
        cacheMutex.lock();
        cacheQueue.enqueue(imgData);
        cacheMutex.unlock();

        // Start thread if not already started
        start();
    }
}

void MediaManager::run()
{
    qCDebug(CHOQOK) << "MediaManager Thread started";

    while (1) {
        cacheMutex.lock();
        ImageFetchData data = cacheQueue.dequeue();
        cacheMutex.unlock();

        QPixmap p;

        if (p.loadFromData(data.data)) {
            // Insert data
            cache.insertPixmap(data.remote.toDisplayString(), p);
            Q_EMIT imageFetched(data.remote, p);
        } else {
            qCCritical(CHOQOK) << "Cannot parse reply from " << data.remote.toDisplayString();
            Q_EMIT fetchError(data.remote, i18n("The request failed. Cannot get image file."));
        }

        cacheMutex.lock();
        if (cacheQueue.isEmpty()) {
            cacheMutex.unlock();
            break;
        } else
            cacheMutex.unlock();
    }
}

void MediaManager::clearImageCache()
{
    cache.clear();
}

QPixmap MediaManager::convertToGrayScale(const QPixmap &pic)
{
    QImage result = pic.toImage();
    for (int y = 0; y < result.height(); ++y) {
        for (int x = 0; x < result.width(); ++x) {
            int pixel = result.pixel(x, y);
            int gray = qGray(pixel);
            int alpha = qAlpha(pixel);
            result.setPixel(x, y, qRgba(gray, gray, gray, alpha));
        }
    }
    return QPixmap::fromImage(result);
}

void MediaManager::uploadMedium(const QUrl &localUrl, const QString &pluginId)
{
    QString pId = pluginId;
    if (pId.isEmpty()) {
        pId = Choqok::BehaviorSettings::lastUsedUploaderPlugin();
    }
    if (pId.isEmpty()) {
        Q_EMIT mediumUploadFailed(localUrl, i18n("No pluginId specified, And last used plugin is null."));
        return;
    }
    if (!uploader) {
        Plugin *plugin = PluginManager::self()->loadPlugin(pId);
        uploader = qobject_cast<Uploader *>(plugin);
    } else if (uploader->pluginId() != pId) {
        //         qCDebug(CHOQOK)<<"CREATING A NEW UPLOADER OBJECT";
        PluginManager::self()->unloadPlugin(uploader->pluginId());
        Plugin *plugin = PluginManager::self()->loadPlugin(pId);
        uploader = qobject_cast<Uploader *>(plugin);
    }
    if (!uploader) {
        return;
    }
    KIO::StoredTransferJob *picJob = KIO::storedGet(localUrl, KIO::Reload, KIO::HideProgressInfo);
    picJob->exec();
    if (picJob->error()) {
        qCritical() << "Job error:" << picJob->errorString();
        KMessageBox::detailedError(UI::Global::mainWindow(), i18n("Uploading medium failed: cannot read the medium file."),
                                   picJob->errorString());
        return;
    }
    const QByteArray picData = picJob->data();
    if (picData.count() == 0) {
        qCritical() << "Cannot read the media file, please check if it exists.";
        KMessageBox::error(UI::Global::mainWindow(), i18n("Uploading medium failed: cannot read the medium file."));
        return;
    }
    connect(uploader, &Uploader::mediumUploaded, this, &MediaManager::mediumUploaded);
    connect(uploader, &Uploader::uploadingFailed, this, &MediaManager::mediumUploadFailed);
    const QMimeDatabase db;
    uploader->upload(localUrl, picData, db.mimeTypeForUrl(localUrl).name().toLocal8Bit());
}

QByteArray MediaManager::createMultipartFormData(const QMap<QString, QByteArray> &formdata,
                                                 const QList<QMap<QString, QByteArray>> &mediaFiles)
{
    QByteArray newLine("\r\n");
    QString formHeader(QLatin1String(newLine) + QLatin1String("Content-Disposition: form-data; name=\"%1\""));
    QByteArray header("--AaB03x");
    QByteArray footer("--AaB03x--");
    QString fileHeader(QLatin1String(newLine) + QLatin1String("Content-Disposition: file; name=\"%1\"; filename=\"%2\""));
    QByteArray data;

    data.append(header);

    if (!mediaFiles.isEmpty()) {
        QList<QMap<QString, QByteArray>>::const_iterator it1 = mediaFiles.constBegin();
        QList<QMap<QString, QByteArray>>::const_iterator endIt1 = mediaFiles.constEnd();
        for (; it1 != endIt1; ++it1) {
            data.append(fileHeader.arg(QLatin1String(it1->value(QLatin1String("name")).data())).arg(QLatin1String(it1->value(QLatin1String("filename")).data())).toUtf8());
            data.append(newLine + "Content-Type: " + it1->value(QLatin1String("mediumType")));
            data.append(newLine);
            data.append(newLine + it1->value(QLatin1String("medium")));
        }
    }

    for (const QString &key : formdata.keys()) {
        data.append(newLine);
        data.append(header);
        data.append(formHeader.arg(key).toLatin1());
        data.append(newLine);
        data.append(newLine + formdata.value(key));
    }
    data.append(newLine);
    data.append(footer);

    return data;
}
