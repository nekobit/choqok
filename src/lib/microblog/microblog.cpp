/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2008-2012 Mehrdad Momeny <mehrdad.momeny@gmail.com>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "microblog/microblog.h"

#include <QMenu>

#include <KLocalizedString>

#include "account/account.h"
#include "account/accountmanager.h"
#include "choqokbehaviorsettings.h"
#include "composerwidget.h"
#include "libchoqokdebug.h"
#include "postwidget.h"
#include "timelinewidget.h"
#include "ui/microblogwidget.h"

using namespace Choqok;

Microblog::Microblog(const QString &componentName, QObject *parent)
    : Plugin(componentName, parent)
{
    qCDebug(CHOQOK);
    m_saveTimelinesTimer = new QTimer(this);
    m_saveTimelinesTimer->setInterval(BehaviorSettings::notifyInterval() * 60000);
    connect(m_saveTimelinesTimer, &QTimer::timeout, this, &Microblog::saveTimelines);
    connect(BehaviorSettings::self(), &BehaviorSettings::configChanged, this,
            &Microblog::slotConfigChanged);
    m_saveTimelinesTimer->start();
}

Microblog::~Microblog()
{
}

QMenu *Microblog::actionsMenuFactory(Account *, QWidget *parent)
{
    return new QMenu(parent);
}

QString Microblog::serviceName() const
{
    return m_serviceName;
}

QString Microblog::homepageUrl() const
{
    return m_homepage;
}

void Microblog::setServiceName(const QString &serviceName)
{
    m_serviceName = serviceName;
}

void Microblog::setServiceHomepageUrl(const QString &homepage)
{
    m_homepage = homepage;
}

QStringList Microblog::timelineNames() const
{
    return m_timelineTypes;
}

void Microblog::setTimelineNames(const QStringList &types)
{
    m_timelineTypes = types;
}

void Microblog::addTimelineName(const QString &name)
{
    m_timelineTypes << name;
}

bool Microblog::isValidTimeline(const QString &timeline)
{
    return m_timelineTypes.contains(timeline);
}

void Microblog::slotConfigChanged()
{
    m_saveTimelinesTimer->setInterval(BehaviorSettings::notifyInterval() * 60000);
}

/// UI Objects:

Account *Microblog::accountFactory(const QString &alias)
{
    Choqok::Account *acc = Choqok::AccountManager::self()->findAccount(alias);
    if (!acc) {
        return new Choqok::Account(this, alias);
    } else {
        return nullptr;
    }
}

UI::MicroblogWidget *Microblog::microblogWidgetFactory(Choqok::Account *account, QWidget *parent)
{
    return new UI::MicroblogWidget(account, parent);
}

UI::ComposerWidget *Microblog::composerWidgetFactory(Account *account, QWidget *parent)
{
    return new UI::ComposerWidget(account, parent);
}

UI::TimelineWidget *Microblog::timelineWidgetFactory(Account *account, const QString &timelineName, QWidget *parent)
{
    return new UI::TimelineWidget(account, timelineName, parent);
}

UI::StatusWidget *Microblog::statusWidgetFactory(Account *account, Choqok::Post *post, QWidget *parent)
{
    return new UI::StatusWidget(account, post, parent);
}

TimelineInfo *Microblog::timelineInfo(const QString &)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
    return nullptr;
}

void Microblog::abortAllJobs(Account *)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
}

void Microblog::abortCreatePost(Account *, Post *)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
}

void Microblog::createPost(Account *, Post *)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
}

void Microblog::fetchPost(Account *, Post *)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
}

void Microblog::removePost(Account *, Post *)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
}

void Microblog::updateTimelines(Account *)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
}

QList<Post *> Microblog::loadTimeline(Account *, const QString &)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
    return QList<Post *>();
}

void Microblog::saveTimeline(Account *, const QString &, const QList<UI::StatusWidget *> &)
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
}

QUrl Microblog::postUrl(Account *, const QString &, const QString &) const
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
    return QUrl();
}

QUrl Microblog::profileUrl(Account *, const QString &) const
{
    qCWarning(CHOQOK) << "Microblog Plugin should implement this!";
    return QUrl();
}
