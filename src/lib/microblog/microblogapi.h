/*
  This file is part of Choqok, the KDE micro-blogging client

  SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include "choqok_export.h"
#include "http/httpapi.h"
#include <QObject>
#include <QString>

namespace Choqok
{

/**
   @brief Mastodon API implementation and base for Pleroma

   @author Nekobit \<me@ow.nekobit.net\>
*/

namespace API
{
class CHOQOK_EXPORT MicroblogApi : public QObject
{
    Q_OBJECT
public:
    MicroblogApi(QString hostUrlString, QObject *parent = nullptr);
    virtual ~MicroblogApi() = default;

protected:
    HttpApi m_api;
};
}
}
