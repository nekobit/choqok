/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2008-2012 Mehrdad Momeny <mehrdad.momeny@gmail.com>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef MICROBLOG_H
#define MICROBLOG_H

#include <QMenu>
#include <QString>
#include <QTimer>

#include "account/account.h"
#include "choqok_export.h"
#include "choqokerrors.h"
#include "choqoktypes.h"
#include "choqokuiglobal.h"
#include "microblogapi.h"
#include "plugin.h"

class ChoqokEditAccountWidget;

namespace Choqok
{
namespace UI
{
class StatusWidget;
class TimelineWidget;
class MicroblogWidget;
class ComposerWidget;
}

/**
 * @brief Base class for Microblog plugins
 *
 * Every Microblog plugin should subclass this they can subclass UI classes to use too, and implement:
 *
 * @ref accountFactory() To create a new account for this microblog, can use @ref Choqok::Account or a subclass of it,
 * @ref EditAccountWidgetFactory() To create a widget/dialog to show to user for create/edit account,
 * @ref microblogWidgetFactory() To create a @ref MicroblogWidget to show on MainWindow
 * @ref timelineWidgetFactory() To create a @ref TimelineWidget to show on MicroblogWidget
 *
 * Additionally should implement this functions:
 * @ref createPost() @ref abortCreatePost() @ref fetchPost() @ref removePost() @ref updateTimelines() @ref profileUrl()
 *
 * @author Mehrdad Momeny \<mehrdad.momeny@gmail.com\>
 **/
class CHOQOK_EXPORT Microblog : public Plugin
{
    Q_OBJECT
public:
    virtual ~Microblog();

    /**
    @brief Return a KMenu contain microblog specific actions.
    Can use for search facilities, or other things
    Will add to a button on top of @ref MicroblogWidget of account/microblog
    */
    virtual QMenu *actionsMenuFactory(Account *theAccount, QWidget *parent = UI::Global::mainWindow());

    /**
     * @brief Create a new Account
     *
     * This method is called during the loading of the config file.
     * @param alias - the alias name to create the account with.
     *
     * you don't need to register the account to the AccountManager in this function.
     *
     * @return The new @ref Account object created by this function
     */
    virtual Account *accountFactory(const QString &alias);

    /**
     * @brief Create a new EditAccountWidget
     *
     * @return A new @ref ChoqokEditAccountWidget to be shown in the account part of the configurations.
     *
     * @param account is the Account to edit. If it's nullptr, then we create a new account
     * @param parent The parent of the 'to be returned' widget
     */
    virtual ChoqokEditAccountWidget *editAccountWidgetFactory(Choqok::Account *account, QWidget *parent) = 0;

    /**
     * @brief Create a MicroblogWidget for this Account
     * The returned MicroblogWidget will show on Mainwindow. and manage of this microblog account will give to it
     * Every Microblog plugin should reimplement this.
     *
     * @return A new MicroblogWidget to use.
     *
     * @param account account to use.
     * @param parent The parent of the 'to be returned' widget
     */
    virtual UI::MicroblogWidget *microblogWidgetFactory(Choqok::Account *account, QWidget *parent);

    /**
     * @brief Create a ComposerWidget to use in MicroblogWidget
     *
     * @return A new ComposerWidget to use.
     *
     * @param account account to use.
     * @param parent The parent of the 'to be returned' widget
     */
    virtual UI::ComposerWidget *composerWidgetFactory(Choqok::Account *account, QWidget *parent);

    /**
     * @brief Create a TimelineWidget to use in MicroblogWidget
     *
     * @return A new TimelineWidget to use.
     *
     * @param account account to use.
     * @param timelineName Name of timeline
     * @param parent The parent of the 'to be returned' widget
     */
    virtual UI::TimelineWidget *timelineWidgetFactory(Choqok::Account *account, const QString &timelineName,
                                                      QWidget *parent);

    /**
     * @brief Create a StatusWidget to contain one post in TimelineWidget
     *
     * @return A new StatusWidget to use.
     *
     * @param account account to use.
     * @param post Post object.
     * @param parent The parent of the 'to be returned' widget
     */
    virtual UI::StatusWidget *statusWidgetFactory(Choqok::Account *account,
                                                  Choqok::Post *post, QWidget *parent);

    /**
    @brief Save a specific timeline!
    @Note Implementation of this is optional, i.e. One microblog may don't have timeline backup

    @see loadTimeline()
    */
    virtual void saveTimeline(Choqok::Account *account, const QString &timelineName,
                              const QList<UI::StatusWidget *> &timeline);
    /**
    @brief Load a specific timeline!
    @Note Implementation of this is optional, i.e. One microblog may don't have timeline backup

    @see saveTimeline()
    */
    virtual QList<Post *> loadTimeline(Choqok::Account *account, const QString &timelineName);

    /**
    \brief Create a new post

    @see postCreated()
    @see abortCreatePost()
    */
    virtual void createPost(Choqok::Account *theAccount, Choqok::Post *post);

    /**
    \brief Abort all requests!
    */
    virtual void abortAllJobs(Choqok::Account *theAccount);

    /**
    @brief Abort all createPost jobs
    @see abortAllJobs()
    */
    virtual void abortCreatePost(Choqok::Account *theAccount, Choqok::Post *post = nullptr);
    /**
    @brief Fetch a post

    @see postFetched()
    */
    virtual void fetchPost(Choqok::Account *theAccount, Choqok::Post *post);

    /**
    @brief Remove a post

    @see postRemoved()
    */
    virtual void removePost(Choqok::Account *theAccount, Choqok::Post *post);

    /**
    Request to update all timelines of account!
    They will arrive in several signals! with timelineDataReceived() signal!

    @see timelineDataReceived()
    */
    virtual void updateTimelines(Choqok::Account *theAccount);

    /**
    return Url to account page on service (Some kind of blog homepage)
    */
    virtual QUrl profileUrl(Choqok::Account *account, const QString &username) const;

    /**
    Provide a Web link for post with id @p postId
    */
    virtual QUrl postUrl(Choqok::Account *account, const QString &username, const QString &postId) const;

    /**
    @return List of timelines supported by this account
    It will use to show timelines! and result of timelineDataReceived() signal will be based on these!

    @see timelineInfo()
    @see timelineDataReceived()
    */
    QStringList timelineNames() const;

    /**
    Checks if @p timeline is valid for this blog! i.e. there is an entry for it at timelineTypes() list.
    @return True if the timeline is valid, false if not!
    */
    bool isValidTimeline(const QString &timelineName);

    /**
    @return information about an specific timeline
    */
    virtual TimelineInfo *timelineInfo(const QString &timelineName);

    /**
    @return service homepage Url
    */
    QString homepageUrl() const;

    /**
       @return a user readable name of blog/service type! (e.g. Identica)
    */
    QString serviceName() const;

    static QString errorString(Choqok::ErrorType type);

Q_SIGNALS:

    /**
    emit when data for a timeline received! @p type specifies the type of timeline as specifies in timelineTypes()
    */
    void timelineDataReceived(Choqok::Account *theAccount, const QString &timelineName,
                              QList<Choqok::Post *> data);

    /**
    emit when a post successfully created!
    */
    void postCreated(Choqok::Account *theAccount, Choqok::Post *post);

    /**
    emit when a post successfully fetched!
    */
    void postFetched(Choqok::Account *theAccount, Choqok::Post *post);

    /**
    emit when a post successfully removed!
    */
    void postRemoved(Choqok::Account *theAccount, Choqok::Post *post);

    /**
    emit when an error occurred the @p errorMessage will specify the error.
    */
    void error(Choqok::Account *theAccount, Choqok::ErrorType error,
               const QString &errorMessage, Choqok::ErrorLevel level = ErrorLevel::Normal);

    /**
    emit when an error occurred on Post manipulation. e.g. On Creation!
    */
    void errorPost(Choqok::Account *theAccount, Choqok::Post *post,
                   Choqok::ErrorType error, const QString &errorMessage,
                   Choqok::ErrorLevel level = Choqok::ErrorLevel::Normal);

    /**
    emit when microblog plugin is going to unload, and @ref Choqok::TimelineWidget should save their timelines
    */
    void saveTimelines();

protected:
    Microblog(const QString &componentName, QObject *parent = nullptr);

    virtual void setTimelineNames(const QStringList &);
    void addTimelineName(const QString &);
    void setServiceName(const QString &);
    void setServiceHomepageUrl(const QString &);

    virtual void createMicroblogApi(){};

    /** General MicroblogApi */
    std::shared_ptr<Choqok::API::MicroblogApi> m_api;

protected Q_SLOTS:
    void slotConfigChanged();

private:
    QString m_serviceName;
    QString m_homepage;
    QStringList m_timelineTypes;
    QTimer *m_saveTimelinesTimer;
};

}

#endif
