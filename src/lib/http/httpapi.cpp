/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "httpapi.h"
#include <KSharedConfig>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <qnetworkaccessmanager.h>
#include <utility>

using namespace Choqok;

HttpApi::HttpApi(QString hostUrlString, QObject *parent /* =nullptr */)
    : QObject(parent)
    , m_hostUrl(std::move(hostUrlString))
    , m_networkManager(this)
{
    // Strip host slash
    m_hostUrl = m_hostUrl.adjusted(QUrl::StripTrailingSlash);

    connect(&m_networkManager, &QNetworkAccessManager::finished, this,
            [&](QNetworkReply *rep) {
                Q_EMIT finished(rep);
            });
}

void HttpApi::finished(QNetworkReply *reply)
{
    reply->deleteLater();
}

void HttpApi::authorizeRequest(QNetworkRequest &request)
{
    if (this->token.isNull()) {
        QString token = QStringLiteral("Bearer %1").arg(this->token);
        request.setRawHeader(QStringLiteral("Authorization").toLatin1(),
                             token.toLatin1());
    }
}

QNetworkReply *HttpApi::requestGet(QString urlString)
{
    QUrl url{m_hostUrl};
    url.setPath(urlString);
    QNetworkRequest req(m_hostUrl);
    return m_networkManager.get(req);
}

QNetworkReply *HttpApi::requestPost(QString urlString)
{
    QUrl url{m_hostUrl};
    url.setPath(urlString);
    QNetworkRequest req(m_hostUrl);
    return m_networkManager.get(req);
}
