/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2022 Nekobit <me@ow.nekobit.net>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include "choqok_export.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QString>

namespace Choqok
{
/**
   @brief Microblog HTTP API REST base

   Microblog plugins or general API wrappers should subclass this.

   @author Nekobit \<me@ow.nekobit.net\>
*/
class CHOQOK_EXPORT HttpApi : public QObject
{
    Q_OBJECT
public:
    HttpApi(QString hostUrlString, QObject *parent = nullptr);
    virtual ~HttpApi() = default;

    inline const QUrl &host() const noexcept
    {
        return m_hostUrl;
    }

    /**
       @brief Sets up any authorization methods on the API

       Default behavior is a bearer token header, `Authorization: Bearer {token}`,
       but can be overriden to use more complex or custom authorization methods

       @param url The QNetworkRequest to get modified
     */
    virtual void authorizeRequest(QNetworkRequest &request);

    QNetworkReply *requestGet(QString urlString);
    // TODO
    QNetworkReply *requestPost(QString urlString);

    /**
       @brief Get the internal QNetworkAccessManager

       HttpApi provides its own slot wrappers around QNetworkAccessManager,
       but only the commonly used ones, so one should prefer those instead of
       directly grabbing the (const) QNetworkAccessManager.

       @return The internal QNetworkAccessManager, mainly for signals.
    */
    inline const QNetworkAccessManager &networkManager() const
    {
        return m_networkManager;
    }

    QString token;
public
    Q_SIGNAL : void finished(QNetworkReply *);

private:
    QUrl m_hostUrl;
    QNetworkAccessManager m_networkManager;
};
}
