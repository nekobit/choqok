/*
  This file is part of Choqok, the KDE micro-blogging client

  SPDX-FileCopyrightText: 2008-2012 Mehrdad Momeny <mehrdad.momeny@gmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef POSTWIDGET_H
#define POSTWIDGET_H

#include <QDateTime>
#include <QVariantAnimation>

#include "account/account.h"
#include "choqoktypes.h"
#include "microblog/microblog.h"
#include "timelinewidget.h"
#include "ui_status.h"

class QAction;
class QPushButton;

namespace Choqok
{
namespace UI
{
/**
   Status widget, kept for compatibility

   @author Nekobit \<me@ow.nekobit.net\> (Rewrote class logic)
   @author Mehrdad Momeny \<mehrdad.momeny@gmail.com\> (Author of original class)
*/
class CHOQOK_EXPORT StatusWidget : public QWidget, private Ui::Status
{
    Q_OBJECT

public:
    explicit StatusWidget(Choqok::Account *account, Choqok::Post *post, QWidget *parent = nullptr);
    virtual ~StatusWidget() = default;
    Post *getPost() const
    {
        return this->postptr;
    }

    /** Return account */
    inline Account *getAccount()
    {
        return this->accountptr;
    }

    // as shrimple as that :)
    void updateUi();

    /**
       Fetches the post content

       @return post content
    */
    inline QString getContent() const
    {
        return this->data_content;
    }

    TimelineWidget *timelineWidget() const
    {
        return timeline;
    }

    /**
       @return true if this is our post, otherwise false
     */
    bool isOwnPost();

    // void slotImageFetched(const QUrl &remoteUrl, const QPixmap &pixmap);

    // void fetchImage();

    // void slotImageFetched(const QUrl &remoteUrl, const QPixmap &pixmap);

    // void setupAvatar();

    void setupAvatar();

    void avatarFetched(const QUrl &remoteUrl, const QPixmap &pixmap);
    void avatarFetchError(const QUrl &remoteUrl, const QString &errMsg);
    void toggleLike(bool);
    void toggleReblog(bool);

    // virtual QString formatDateTime(const QDateTime &time);

private:
    QString data_content;
    QString image;
    QUrl imageUrl;
    QString dir;
    QPixmap originalImage;
    QString extraContents;
    Choqok::Post *postptr;
    Choqok::Account *accountptr;

    TimelineWidget *timeline;
    static const QLatin1String resourceImageUrl;
};
}
}
#endif // POSTWIDGET_H
