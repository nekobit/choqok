/*
    This file is part of Choqok, the KDE micro-blogging client

    SPDX-FileCopyrightText: 2008-2012 Mehrdad Momeny <mehrdad.momeny@gmail.com>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "postwidget.h"

#include <QCloseEvent>
#include <QGridLayout>
#include <QPushButton>
#include <QTimer>

#include <KLocalizedString>
#include <KMessageBox>

#include "choqokappearancesettings.h"
#include "choqokbehaviorsettings.h"
#include "choqoktools.h"
#include "choqokuiglobal.h"
#include "libchoqokdebug.h"
#include "mediamanager.h"
#include "quickpost.h"
#include "textbrowser.h"
#include "timelinewidget.h"
#include "urlutils.h"

static const int _15SECS = 15000;
static const int _MINUTE = 60000;
static const int _HOUR = 60 * _MINUTE;

using namespace Choqok;
using namespace Choqok::UI;

namespace
{
QIcon likedIcon;
QIcon rebloggedIcon;

QPixmap fillNonAlphaColors(const QPixmap &p, QColor color)
{
    QImage pic = p.toImage();
    QRgb colorRgb = color.rgba();

    for (int y = 0; y < pic.height(); ++y) {
        QRgb *line = reinterpret_cast<QRgb *>(pic.scanLine(y));
        for (int x = 0; x < pic.width(); ++x) {
            QRgb &rgb = line[x];
            if (qAlpha(rgb) != 0)
                rgb = qRgba(qRed(colorRgb),
                            qGreen(colorRgb),
                            qBlue(colorRgb),
                            qAlpha(rgb)); // Keep alpha to maintain smoothing
        }
    }
    return QPixmap::fromImage(pic);
}

void setupIcons()
{
    QPixmap p;
    // TODO dont hardcode 128
    if (likedIcon.isNull()) {
        likedIcon = QIcon::fromTheme(QStringLiteral("starred-symbolic"));
        p = likedIcon.pixmap(128, 128);
        likedIcon = QIcon(fillNonAlphaColors(p, QColor::fromRgb(232, 214, 10)));
    }
    if (rebloggedIcon.isNull()) {
        rebloggedIcon = QIcon::fromTheme(QStringLiteral("retweet"));
        p = rebloggedIcon.pixmap(128, 128);
        rebloggedIcon = QIcon(fillNonAlphaColors(p, QColor::fromRgb(93, 210, 74)));
    }
}
}

StatusWidget::StatusWidget(Account *account, Choqok::Post *post, QWidget *parent /* = 0*/)
    : QWidget(parent)
    , postptr{post}
    , accountptr{account}
{
    // Run static icon check
    setupIcons();

    setupUi(this);

    // this->setFrameShape(QFrame::NoFrame);
    // setAttribute(Qt::WA_DeleteOnClose);
    // _mainWidget->setFrameShape(QFrame::NoFrame);
    // if (isOwnPost()) {
    //     d->mCurrentPost->isRead = true;
    // }
    // d->mTimer.start(_MINUTE);
    // connect(&d->mTimer, &QTimer::timeout, this, &StatusWidget::updateUi);
    // connect(_mainWidget, &TextBrowser::clicked, this, &StatusWidget::mousePressEvent);
    // connect(_mainWidget, &TextBrowser::anchorClicked, this, &StatusWidget::checkAnchor);
    // connect(content, &TextBrowser::textChanged, this, &StatusWidget::setHeight);
    connect(like_btn, &QPushButton::clicked, this, &StatusWidget::toggleLike);
    connect(reblog_btn, &QPushButton::clicked, this, &StatusWidget::toggleReblog);

    timeline = qobject_cast<TimelineWidget *>(parent);
    setupAvatar();
    updateUi();
}

/*
  Cool spin animation, but quite performance hungry. (and uses more memory)
  Could probably reimplement this using a QImage
  Fun while I did it, but not practical at all :-)

    likeAnim.setDuration(1000);
    likeAnim.setStartValue(0.0f);
    likeAnim.setEndValue(360.0f*2.0f);// 4 spins
    connect(&likeAnim, &QVariantAnimation::valueChanged, this, &StatusWidget::animateSpin);

    void StatusWidget::animateSpin(const QVariant &value)
    {
        qCDebug(CHOQOK) << value;
        float frame = value.toFloat();
        QTransform t;
        t.rotate(value.toReal());
        like_btn->setIcon(QIcon(likedIcon.pixmap(128, 128).transformed(t)));
    }
*/

bool StatusWidget::isOwnPost()
{
    return getAccount()->username().compare(getPost()->author.userName, Qt::CaseInsensitive) == 0;
}

void StatusWidget::updateUi()
{
    display_name->setText(postptr->author.realName);
    account->setText(postptr->author.userName);
    content->setText(postptr->content);
}

void StatusWidget::toggleLike(bool checked)
{
    if (!checked) {
        // Return to default icon
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("non-starred-symbolic");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        like_btn->setIcon(icon);
    } else {
        // Use liked and animate spin
        like_btn->setIcon(likedIcon);
    }
}

void StatusWidget::toggleReblog(bool checked)
{
    if (!checked) {
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("retweet");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        reblog_btn->setIcon(icon);
    } else {
        reblog_btn->setIcon(rebloggedIcon);
    }
}

// void StatusWidget::fetchImage()
// {
//     if (imageUrl.isEmpty()) {
//         return;
//     }

//     QPixmap pix = MediaManager::self()->fetchImage(imageUrl, MediaManager::Async);

//     if (!pix.isNull()) {
//         slotImageFetched(imageUrl, pix);
//     } else {
//         connect(MediaManager::self(), &MediaManager::imageFetched, this,
//                 &StatusWidget::slotImageFetched);
//     }
// }

// void StatusWidget::slotImageFetched(const QUrl &remoteUrl, const QPixmap &pixmap)
// {
//     if (remoteUrl == imageUrl) {
//         disconnect(MediaManager::self(), &MediaManager::imageFetched, this, &StatusWidget::slotImageFetched);
//         originalImage = pixmap;
//         updatePostImage( width() );
//         updateUi();
//     }
// }

void StatusWidget::setupAvatar()
{
    QPixmap pix = MediaManager::self()->fetchImage(postptr->author.profileImageUrl);
    if (!pix.isNull()) {
        avatarFetched(postptr->author.profileImageUrl, pix);
    } else {
        connect(MediaManager::self(), &MediaManager::imageFetched, this, &StatusWidget::avatarFetched);
        connect(MediaManager::self(), &MediaManager::fetchError, this, &StatusWidget::avatarFetchError);
    }
}

#include <iostream>
void StatusWidget::avatarFetched(const QUrl &remoteUrl, const QPixmap &pixmap)
{
    if (remoteUrl == postptr->author.profileImageUrl) {
        avatar->setScaledContents(false);
        avatar->setPixmap(pixmap.scaled(avatar->width(), avatar->height(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
        updateUi();
        disconnect(MediaManager::self(), &MediaManager::imageFetched, this, &StatusWidget::avatarFetched);
        disconnect(MediaManager::self(), &MediaManager::fetchError, this, &StatusWidget::avatarFetchError);
    }
}

void StatusWidget::avatarFetchError(const QUrl &remoteUrl, const QString &errMsg)
{
    // Q_UNUSED(errMsg);
    // if (remoteUrl == d->mCurrentPost->author.profileImageUrl) {
    //     ///Avatar fetching is failed! but will not disconnect to get the img if it fetches later!
    //     const QUrl url(QLatin1String("img://profileImage"));
    //     _mainWidget->document()->addResource(QTextDocument::ImageResource,
    //                                          url, QIcon::fromTheme(QLatin1String("image-missing")).pixmap(48));
    //     updateUi();
    // }
}

// void StatusWidget::deleteLater()
// {
//     close();
// }
