#pragma once
#include <QString>

/**
    Enumeration for possible errors.
*/
namespace Choqok
{
enum class ErrorType {
    /** A server side error. */
    ServerError,
    /** An error on communication with server */
    CommunicationError,
    /** A parsing error. */
    ParsingError,
    /** An error on authentication. */
    AuthenticationError,
    /** An error where the method called is not supported by this object. */
    NotSupportedError,
    /** Any other miscellaneous error. */
    OtherError
};

/**
   Enumeration for levels of errors.
   This could use for user feedback!
   For @ref Critical errors will show a messagebox,
   For @ref Normal will show a knotify.
   For @ref Low just show a message in statusBar
*/
enum class ErrorLevel {
    Low,
    Normal,
    Critical
};

QString errorString(Choqok::ErrorType type);
}
