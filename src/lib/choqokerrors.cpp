#include "choqokerrors.h"
#include <KLocalizedString>

QString Choqok::errorString(Choqok::ErrorType type)
{
    using ErrorType = Choqok::ErrorType;

    switch (type) {
    case ErrorType::ServerError:
        return i18n("The server returned an error");
    case ErrorType::CommunicationError:
        return i18n("Error on communication with server");
    case ErrorType::ParsingError:
        return i18n("Error on parsing results");
    case ErrorType::AuthenticationError:
        return i18n("Authentication error");
    case ErrorType::NotSupportedError:
        return i18n("The server does not support this feature");
    default:
    case ErrorType::OtherError:
        return i18n("Unknown error");
    }
    return QString();
}
