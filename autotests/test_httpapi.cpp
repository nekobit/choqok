#include "http/httpapi.h"
#include <QDebug>
#include <QtCore>
#include <QtTest>
#include <qnetworkaccessmanager.h>
#include <qstringliteral.h>

class testHttpApi : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void simpleRequest();
};

#include <iostream>

void testHttpApi::simpleRequest()
{
    Choqok::HttpApi api{QStringLiteral("https://mastodon.social"), this};
    api.requestGet(QStringLiteral("/api/v1/timelines/public"));

    connect(&api, &Choqok::HttpApi::finished,
            [&](QNetworkReply *rep) {
                // Success!
            });
}

QTEST_MAIN(testHttpApi)
#include "test_httpapi.moc"
